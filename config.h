/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int gappx     = 10;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = {
	"sans-serif:size=12:antialias=true:autohint=true",
	"Twemoji:pixelsize=14:antialias=true:autohint=true"
};
static const char dmenufont[]       = "sans-serif:size=12";
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     instance  title  tags mask  isfloating  isterminal  noswallow  monitor */
	{ "qutebrowser",  NULL, NULL, 1 << 1,            0,          0,         0,       -1 },
	{ "discord",      NULL, NULL, 1 << 7,            0,          0,         0,       -1 },
	{ "Spotify",      NULL, NULL, 1 << 6,            0,          0,         0,       -1 },
	{ "Telegram",     NULL, NULL, 1 << 8,            0,          0,         0,       -1 },
	{ "Nextcloud",    NULL, NULL, 1 << 8,            0,          0,         0,       -1 },
	/* { "Nextcloud",    NULL, NULL,      0,            1,          0,         0,       -1 }, */
	{ "jetbrains-studio",    NULL, NULL,      0,            1,          0,         0,       -1 },
	{ NULL, "st",           NULL,      0,            0,          1,        -1,       -1 },
	{ NULL, NULL, "Event Tester",      0,            0,          0,         1,       -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.6;  /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int attachbelow = 1;    /* 1 means attach after the currently active window */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#include "bstack.c"
#include "centered.c"
#include "deck.c"
#include "fibonacci.c"
#include "gaplessgrid.c"
#include "movestack.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "tile", tile },
	{ "deck", deck },
	{ "mono", monocle },
	{ "top", bstack },
	{ "wide", bstackhoriz },
	{ "center", centeredmaster },
 	{ "spiral", spiral },
 	{ "grid", gaplessgrid },
	{ "float", NULL },
	{ NULL, NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "rofi", "-show-icons", "-show", "drun", "-p", "programs", NULL };
static const char *termcmd[]  = { "st", NULL };

/* keyboard shortcuts defined using sxhkd */
static Key keys[] = {
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button2,        setlayout,      {.v = &layouts[0]} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkLtSymbol,          0,              Button4,        cyclelayout,    {.i = -1 } },
	{ ClkLtSymbol,          0,              Button5,        cyclelayout,    {.i = +1 } },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkWinTitle,          0,              Button3,        spawn,          {.v = dmenucmd } },
	{ ClkWinTitle,          0,              Button4,        focusstack,     {.i = -1 } },
	{ ClkWinTitle,          0,              Button5,        focusstack,     {.i = +1 } },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkStatusText,        0,              Button3,        spawn,          SHCMD("power-menu") },
	{ ClkStatusText,        0,              Button4,        spawn,          SHCMD("audio volume up 1") },
	{ ClkStatusText,        0,              Button5,        spawn,          SHCMD("audio volume down 1") },
	{ ClkStatusText,        MODKEY,         Button4,        spawn,          SHCMD("brightness up 5") },
	{ ClkStatusText,        MODKEY,         Button5,        spawn,          SHCMD("brightness down 5") },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            0,              Button4,        cycleview,      {.i = -1 } },
	{ ClkTagBar,            0,              Button5,        cycleview,      {.i = +1 } },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkTagBar,            MODKEY,         Button4,        cycletags,      {.i = -1 } },
	{ ClkTagBar,            MODKEY,         Button5,        cycletags,      {.i = +1 } },
};

/* signal definitions */
/* signum must be greater than 0 */
/* trigger signals using `xsetroot -name "&<signame> [<type> <value>]"` */
/* where type is one of: */
/* 	i: int */
/* 	ui: unsigned int */
/* 	f: float */
/* 	t: tag number, for t=1-9: 1 << (t-1); for t=0: ~0 */
/* 	l: index in Layout layouts[] */
#define SIG(FUNCTION) { #FUNCTION, FUNCTION }
static Signal signals[] = {
	SIG(cyclelayout), // i offset
	SIG(cycletags), // i offset
	SIG(cycleview), // i offset
	SIG(focusmon), // i offset
	SIG(focusstack), // i offset
	SIG(incnmaster), // i offset
	SIG(killclient),
	SIG(movestack), // i offset
	SIG(resetlayout),
	SIG(quit), // i=0 to quit, i=1 to restart
	SIG(setgaps), // i offset, ui=1 to toggle
	SIG(setlayout), // l or none to toggle
	SIG(setmfact), // f change
	SIG(tag), // t tag or none to toggle
	SIG(tagmon), // i offset
	SIG(toggletag), // t tag
	SIG(togglebar),
	SIG(togglefloating),
	SIG(toggleview), // t tag
	SIG(togglewindowedfullscreen),
	SIG(view), // t tag or none to toggle
	SIG(xrdb),
	SIG(zoom),
};
